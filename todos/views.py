from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoItem, TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


# Function view of TodoListDetailView
# def show_todolists(request):
#   context = {"todolists": TodoList.objects.all()}
#   return render(request, "todos/list.html", context)


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self):
        return f"/todos/{self.object.pk}"


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return f"/todos/{self.object.pk}"


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/createitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return f"/todos/{self.object.list.pk}"


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/updateitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return f"/todos/{self.object.list.pk}"
